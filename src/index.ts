import TimeManager from './Managers/TimeManager';
import SizeManager from './Managers/SizeManager';
import InputManager from './Managers/InputManager';
import GameManager from './Managers/GameManager';

// instantiate all the managers
TimeManager.getInstance();
SizeManager.getInstance();
InputManager.getInstance();
GameManager.getInstance();
