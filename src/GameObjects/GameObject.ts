import * as THREE from 'three';
import GameManager from '../Managers/GameManager';
import InputManager from '../Managers/InputManager';
import ResourceManager from '../Managers/Resources/ResourceManager';
import TimeManager from '../Managers/TimeManager';

export default class GameObject {
  gameManager: GameManager;
  timeManager: TimeManager;
  resourceManager: ResourceManager;
  inputManager: InputManager;

  object?: THREE.Object3D;

  constructor() {
    this.gameManager = GameManager.getInstance();
    this.timeManager = TimeManager.getInstance();
    this.resourceManager = ResourceManager.getInstance();
    this.inputManager = InputManager.getInstance();
  }

  addToScene(): void {
    if (!(this.object instanceof THREE.Object3D)) {
      return;
    }
    this.gameManager.scene.add(this.object);
  }

  removeFromScene(): void {
    if (!(this.object instanceof THREE.Object3D)) {
      return;
    }
    this.gameManager.scene.remove(this.object);
  }

  update(): void {
    // do nothing
  }
}
