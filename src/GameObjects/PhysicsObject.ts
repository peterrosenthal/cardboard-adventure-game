import * as CANNON from 'cannon-es';
import * as THREE from 'three';
import GameObject from './GameObject';

export default class PhysicsObject extends GameObject {
  body?: CANNON.Body;

  constructor() {
    super();
  }

  addToWorld(): void {
    if (!(this.body instanceof CANNON.Body)) {
      return;
    }
    this.gameManager.world.addBody(this.body);
  }

  addToWorldAndScene(): void {
    this.addToWorld();
    this.addToScene();
  }

  removeFromWorld(): void {
    if (!(this.body instanceof CANNON.Body)) {
      return;
    }
    this.gameManager.world.removeBody(this.body);
  }

  removeFromWorldAndScene(): void {
    this.removeFromWorld();
    this.removeFromScene();
  }

  syncWorldToScene(): void {
    if (!(this.body instanceof CANNON.Body
       && this.object instanceof THREE.Object3D)) {
      return;
    }
    this.object.position.copy(this.body.position as unknown as THREE.Vector3);
    this.object.quaternion.copy(this.body.quaternion as unknown as THREE.Quaternion);
  }
}
