import * as THREE from 'three';
import EventEmitter from '../../Utils/EventEmitter';
import GameManager from '../../Managers/GameManager';

export default class PressurePlate extends EventEmitter {
  static RAYCASTER_DENSITY = 9;
  
  private gameManager: GameManager;

  object: THREE.Mesh;
  raycasters: THREE.Raycaster[];
  position: THREE.Vector3;
  size: THREE.Vector3;
  pressed: boolean;

  constructor(position: THREE.Vector3, size: THREE.Vector3) {
    super();

    this.position = position;
    this.size = size;
    this.pressed = false;

    this.gameManager = GameManager.getInstance();

    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshStandardMaterial({ color: 0xbafc3a });
    this.object = new THREE.Mesh(geometry, material);
    this.object.scale.copy(size);
    this.object.position.copy(position);

    this.gameManager.scene.add(this.object);

    const raycasterSpacingX = size.x /
      Math.ceil(size.x / (1 / PressurePlate.RAYCASTER_DENSITY));
    const raycasterSpacingZ = size.z /
      Math.ceil(size.z / (1 / PressurePlate.RAYCASTER_DENSITY));
    this.raycasters = [];
    for (
      let x = this.position.x - this.size.x / 2;
      x <= this.position.x + this.size.x / 2;
      x += raycasterSpacingX
    ) {
      for (
        let z = this.position.z - this.size.z / 2;
        z <= this.position.z + this.size.z / 2;
        z += raycasterSpacingZ
      ) {
        this.raycasters.push(new THREE.Raycaster(
          new THREE.Vector3(x, this.position.y - 1, z),
          new THREE.Vector3(0, 1, 0),
          0,
          1.1,
        ));
      }
    }
  }

  update(): void {
    const wasPressed = this.pressed;
    this.pressed = false;
    for (const raycaster of this.raycasters) {
      const intersects = raycaster.intersectObjects(this.gameManager.scene.children);
      if (intersects.length > 0) {
        for (const intersect of intersects) {
          if (intersect.object !== this.object &&
            intersect.object !== this.gameManager.floor?.object &&
            !intersect.object.name.includes('chainsegment')) {
            this.pressed = true;
          }
        }
      }
    }

    if (this.pressed) {
      this.object.position.y = this.position.y - this.object.scale.y / 2;
      if (!wasPressed) {
        this.trigger('pressed');
      }
    } else {
      this.object.position.y = this.position.y;
      if (wasPressed) {
        this.trigger('unpressed');
      }
    }
  }
}
