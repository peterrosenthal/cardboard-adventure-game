import * as CANNON from 'cannon-es';
import * as THREE from 'three';
import PhysicsObject from '../PhysicsObject';
import { exampleObstacleMaterial } from '../../Utils/PhysicsMaterials';

export default class ExampleObstacle extends PhysicsObject {
  constructor() {
    super();

    const size = {
      x: 1,
      y: 1,
      z: 4,
    };
    const position = {
      x: 4,
      y: 4,
      z: 4,
    };
    
    // cannon body
    this.body = new CANNON.Body({
      mass: 1,
      angularFactor: new CANNON.Vec3(0, 1, 0),
      material: exampleObstacleMaterial,
    });
    this.body.addShape(new CANNON.Box(new CANNON.Vec3(
      size.x / 2,
      size.y / 2,
      size.z / 2,
    )));
    this.body.position.copy(position as CANNON.Vec3);
  
    // three object
    this.object = new THREE.Mesh(
      new THREE.BoxGeometry(),
      new THREE.MeshStandardMaterial({
        color: 0x7a69b9,
        roughness: 0.8,
      }),
    );
    this.object.scale.copy(size as THREE.Vector3);
    this.object.position.copy(position as THREE.Vector3);
  }

  override removeFromScene(): void {
    if (this.object instanceof THREE.Mesh) {
      this.object.geometry.dispose();
      if (this.object.material instanceof THREE.Material) {
        this.object.material.dispose();
      } else {
        for (const material of this.object.material) {
          material.dispose();
        }
      }
      this.gameManager.scene.remove(this.object);
    }
  }
}
