import * as THREE from 'three';
import GameObject from '../GameObject';

export default class Lights extends GameObject {
  ambient: THREE.AmbientLight;
  directional: THREE.DirectionalLight;

  constructor() {
    super();

    this.directional = new THREE.DirectionalLight(0xffffff, 0.5);
    this.directional.position.set(3.5, 4, 1.5);

    this.ambient = new THREE.AmbientLight(0xffffff, 0.5);

    this.object = new THREE.Group();
    this.object.add(this.directional, this.ambient);
  }
}
