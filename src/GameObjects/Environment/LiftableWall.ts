import * as CANNON from 'cannon-es';
import * as THREE from 'three';
import PhysicsObject from '../PhysicsObject';
import { floorMaterial } from '../../Utils/PhysicsMaterials';

export default class LiftableWall extends PhysicsObject {
  static MOVE_SPEED = 0.003;

  size: THREE.Vector3;
  position: THREE.Vector3;

  liftable: boolean;
  lifting: boolean;
  liftAmount: number;

  constructor(position: THREE.Vector3, size: THREE.Vector3, liftable = false) {
    super();

    this.size = size;
    this.position = position;

    this.body = new CANNON.Body({
      type: CANNON.BODY_TYPES.STATIC,
      shape: new CANNON.Box(size.clone().divideScalar(2) as unknown as CANNON.Vec3),
      mass: 100,
      angularFactor: new CANNON.Vec3(0, 1, 0),
      material: floorMaterial,
      collisionFilterGroup: 2,
      collisionFilterMask: 1,
      fixedRotation: true,
    });
    // this.body.addShape();
    this.body.position.copy(position as unknown as CANNON.Vec3);

    this.object = new THREE.Mesh(
      new THREE.BoxGeometry(),
      new THREE.MeshStandardMaterial({
        color: 0x7a69b9,
        roughness: 0.8,
      }),
    );
    this.object.scale.copy(size);
    this.object.position.copy(position);

    if (liftable) {
      this.moveUp = this.moveUp.bind(this);
      this.moveDown = this.moveDown.bind(this);
      this.gameManager.pressurePlate?.on('pressed', this.moveUp);
      this.gameManager.pressurePlate?.on('unpressed', this.moveDown);
    }

    this.liftable = liftable;
    this.lifting = false;
    this.liftAmount = 0;
  }

  override update(): void {
    if (this.object === undefined || this.body === undefined) {
      return;
    }
    if (this.body.type === CANNON.BODY_TYPES.DYNAMIC &&
        this.body.position.y < this.size.y / 2 + 0.01) {
      this.body.type = CANNON.BODY_TYPES.STATIC;
      this.body.position.y = this.position.y;
    }
    if (this.lifting) {
      this.liftAmount = THREE.MathUtils.clamp(
        this.liftAmount + (LiftableWall.MOVE_SPEED / this.size.y) * this.timeManager.delta,
        0,
        1,
      );
      this.object.position.y =
        THREE.MathUtils.lerp(this.position.y, this.position.y  + this.size.y, this.liftAmount);
      this.body.position.y = this.object.position.y;
    } else if (this.liftAmount > 0) {
      this.liftAmount = THREE.MathUtils.clamp(
        (this.object.position.y - this.position.y) / this.size.y,
        0,
        1,
      );
    }
    if (this.liftable) {
      this.body.position.x = this.position.x;
      this.body.position.z = this.position.z;
    }
  }

  private moveUp(): void {
    if (this.object === undefined || this.body === undefined) {
      return;
    }
    this.body.type = CANNON.BODY_TYPES.STATIC;
    this.lifting = true;
  }

  private moveDown(): void {
    if (this.object === undefined || this.body === undefined) {
      return;
    }
    this.body.type = CANNON.BODY_TYPES.DYNAMIC;
    this.lifting = false;
  }
}
