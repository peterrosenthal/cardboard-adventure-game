import * as CANNON from 'cannon-es';
import * as THREE from 'three';
import PhysicsObject from '../PhysicsObject';
import { floorMaterial } from '../../Utils/PhysicsMaterials';

export default class Floor extends PhysicsObject {
  constructor () {
    super();

    const size = {
      x: 10,
      y: 1,
      z: 30,
    };
    const position = {
      x: 0,
      y: -0.5,
      z: 0,
    };

    // cannon body
    this.body = new CANNON.Body({
      type: CANNON.BODY_TYPES.STATIC,
      shape: new CANNON.Box(new CANNON.Vec3(
        size.x / 2,
        size.y / 2,
        size.z / 2,        
      )),
      material: floorMaterial,
    });
    this.body.position.copy(position as CANNON.Vec3);

    const aspectRatio = size.x / size.z;
    const texture = this.resourceManager.items.floorGrassTexture as THREE.Texture;
    texture.repeat.x = aspectRatio * 3;
    texture.repeat.y = 3;
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;

    // three object
    this.object = new THREE.Mesh(
      new THREE.BoxGeometry(),
      new THREE.MeshStandardMaterial({
        map: texture,
      }),
    );
    this.object.scale.copy(size as THREE.Vector3);
    this.object.position.copy(position as THREE.Vector3);
  }

  override removeFromScene(): void {
    if (this.object instanceof THREE.Mesh) {
      this.object.geometry.dispose();
      if (this.object.material instanceof THREE.Material) {
        this.object.material.dispose();
      } else {
        for (const material of this.object.material) {
          material.dispose();
        }
      }
      this.gameManager.scene.remove(this.object);
    }
  }
}
