import * as CANNON from 'cannon-es';
import * as THREE from 'three';
import MovementState from './MovementState';
import PhysicsObject from '../PhysicsObject';
import { playerMaterial } from '../../Utils/PhysicsMaterials';
import GrapplingHook from './GrapplingHook/GrapplingHook';
import ExtendoLegs from './ExtendoLegs';

export default class Player extends PhysicsObject {
  // attributes
  static SIZE = 1;
  static MOVEMENT_FORCE = 5.75;
  static MAX_SPEED = 6;
  static CAM_FOLLOW_DIST = 2;
  static CAM_FOLLOW_EASE = 12;
  static ROTATION_SPEED = Math.PI / 900;

  // private member variables
  private movementState: MovementState;

  // grappling hook
  grapplingHook?: GrapplingHook;

  // extendo legs
  extendoLegs?: ExtendoLegs;

  constructor() {
    super();

    // cannon body
    this.body = new CANNON.Body({
      mass: 0.25,
      fixedRotation: true,
      material: playerMaterial,
    });
    this.body.addShape(new CANNON.Sphere(Player.SIZE / 2));
    this.body.position.set(0, 2, 0);

    // three object
    const textures = this.resourceManager.items.playerArrayTextures as THREE.Texture[];
    const materials: THREE.Material[] = [];
    // have to use "old school" for loop to ensure order is preserved
    for (let i = 0; i < textures.length; i++) {
      const texture = textures[i];
      materials.push(new THREE.MeshStandardMaterial({
        map: texture,
        roughness: 0.6,
      }));
    }
    this.object = new THREE.Mesh(
      new THREE.BoxGeometry(),
      materials,
    );
    this.object.scale.set(Player.SIZE, Player.SIZE, Player.SIZE);
    this.object.position.set(0, 2, 0);
    this.object.name = 'player';

    // movement
    this.movementState = {
      forward: 0,
      left: 0,
      back: 0,
      right: 0,
      counterclockwise: 0,
      clockwise: 0,
    };

    // input manager
    this.setMovementForward = this.setMovementForward.bind(this);
    this.setMovementLeft = this.setMovementLeft.bind(this);
    this.setMovementBack = this.setMovementBack.bind(this);
    this.setMovementRight = this.setMovementRight.bind(this);
    this.setMovementCounterclockwise = this.setMovementCounterclockwise.bind(this);
    this.setMovementClockwise = this.setMovementClockwise.bind(this);
    this.inputManager.on('forward.player', this.setMovementForward);
    this.inputManager.on('left.player', this.setMovementLeft);
    this.inputManager.on('back.player', this.setMovementBack);
    this.inputManager.on('right.player', this.setMovementRight);
    this.inputManager.on('counterclockwise.player', this.setMovementCounterclockwise);
    this.inputManager.on('clockwise.player', this.setMovementClockwise);

    // grappling hook
    this.grapplingHook = new GrapplingHook(this);
    this.equipGrapple = this.equipGrapple.bind(this);
    this.inputManager.on('equip.grapple', this.equipGrapple);

    // extendo legs
    // this.extendoLegs = new ExtendoLegs(this);
    this.equipExtendoLegs = this.equipExtendoLegs.bind(this);
    this.inputManager.on('equip.legs', this.equipExtendoLegs);
  }

  override removeFromScene(): void {
    if (this.object instanceof THREE.Mesh) {
      this.object.geometry.dispose();
      if (this.object.material instanceof THREE.Material) {
        this.object.material.dispose();
      } else {
        for (const material of this.object.material) {
          material.dispose();
        }
      }
      this.gameManager.scene.remove(this.object);
    }
  }

  override update(): void {
    if (this.body === undefined || this.object === undefined) {
      return;
    }

    if (this.body.position.y < 0) {
      this.body.position.set(0, Player.SIZE / 2, 0);
      this.body.velocity.set(0, 0, 0);
      this.object.position.set(0, Player.SIZE / 2, 0);
    }

    // rotate ccw or cw
    // time manager's delta is needed because we aren't properly using forces for this
    const rotationAmmount = this.movementState.counterclockwise - this.movementState.clockwise;
    const currentRotation = new CANNON.Vec3();
    this.body.quaternion.toEuler(currentRotation);
    this.body.quaternion.setFromEuler(
      currentRotation.x,
      currentRotation.y + rotationAmmount * Player.ROTATION_SPEED * this.timeManager.delta,
      currentRotation.z,
    );

    // movement
    const movementVector = new THREE.Vector2(
      THREE.MathUtils.clamp(this.movementState.right + this.movementState.back
        - this.movementState.left - this.movementState.forward, -1, 1),
      THREE.MathUtils.clamp(this.movementState.back + this.movementState.left
        - this.movementState.forward - this.movementState.right, -1, 1),
    );
    if (this.body.velocity.length() < Player.MAX_SPEED) {
      this.body.applyForce(
        new CANNON.Vec3(
          movementVector.x * Player.MOVEMENT_FORCE,
          0,
          movementVector.y * Player.MOVEMENT_FORCE,
        ),
        new CANNON.Vec3(0, 0, 0),
      );
    }

    // update the grappling hook
    this.grapplingHook?.update();
  }

  dragCamera(): void {
    const adjustedCamPos = (this.object?.position.clone() ?? new THREE.Vector3())
      .sub(this.gameManager.camera.position
        .clone()
        .sub(new THREE.Vector3(2.5, 0, 2.5)));
    // camera movement has been adjusted to work on a 45 degree offset grid in
    // order to accomodate the isometric camera
    // move left
    if ((adjustedCamPos.x - adjustedCamPos.z) / 2 < -Player.CAM_FOLLOW_DIST) {
      const diff = ((adjustedCamPos.x - adjustedCamPos.z) / 2
        + Player.CAM_FOLLOW_DIST) / Player.CAM_FOLLOW_EASE;
      this.gameManager.camera.position.x += diff;
      this.gameManager.camera.position.z -= diff;
    }
    // move right
    if ((adjustedCamPos.x - adjustedCamPos.z) / 2 > Player.CAM_FOLLOW_DIST) {
      const diff = ((adjustedCamPos.x - adjustedCamPos.z) / 2
        - Player.CAM_FOLLOW_DIST) / Player.CAM_FOLLOW_EASE;
      this.gameManager.camera.position.x += diff;
      this.gameManager.camera.position.z -= diff;
    }
    // move up
    if ((adjustedCamPos.z + adjustedCamPos.x) / 2 < -Player.CAM_FOLLOW_DIST) {
      const diff = ((adjustedCamPos.z + adjustedCamPos.x) / 2
        + Player.CAM_FOLLOW_DIST) / Player.CAM_FOLLOW_EASE;
      this.gameManager.camera.position.x += diff / 2;
      this.gameManager.camera.position.z += diff / 2;
    }
    // move down
    if ((adjustedCamPos.z + adjustedCamPos.x) / 2  > Player.CAM_FOLLOW_DIST) {
      const diff = ((adjustedCamPos.z + adjustedCamPos.x) / 2
        - Player.CAM_FOLLOW_DIST) / Player.CAM_FOLLOW_EASE;
      this.gameManager.camera.position.x += diff / 2;
      this.gameManager.camera.position.z += diff / 2;
    }
  }

  private setMovementForward(value: number) {
    this.movementState.forward = value;
  }

  private setMovementLeft(value: number) {
    this.movementState.left = value;
  }

  private setMovementBack(value: number) {
    this.movementState.back = value;
  }

  private setMovementRight(value: number) {
    this.movementState.right = value;
  }

  private setMovementCounterclockwise(value: number) {
    this.movementState.counterclockwise = value;
  }

  private setMovementClockwise(value: number) {
    this.movementState.clockwise = value;
  }

  private equipGrapple(): void {
    if (this.extendoLegs instanceof ExtendoLegs) {
      this.extendoLegs.destroy();
      this.extendoLegs = undefined;
    }
    this.grapplingHook = new GrapplingHook(this);
  }

  private equipExtendoLegs(): void {
    if (this.grapplingHook instanceof GrapplingHook) {
      this.grapplingHook.destroy();
      this.grapplingHook = undefined;
    }
    this.extendoLegs = new ExtendoLegs(this);
  }
}
