interface MovementState {
  forward: number,
  left: number,
  back: number,
  right: number,
  counterclockwise: number,
  clockwise: number,
}

export default MovementState;
