import * as THREE from 'three';
import * as CANNON from 'cannon-es';
import GameManager from '../../Managers/GameManager';
import InputManager from '../../Managers/InputManager';
import Player from './Player';

export default class ExtendoLegs {
  static MAX_SEGMENTS = 6;
  static SEGMENT_LENGTH = 0.8;
  static JUMPING_FORCE = 20;

  private player: Player;
  private gameManager: GameManager;
  private inputManager: InputManager;

  currentSegments: number;

  parentGroup: THREE.Group;
  leftGroup: THREE.Group;
  rightGroup: THREE.Group;
  leftLeg: THREE.Mesh;
  rightLeg: THREE.Mesh;
  leftFoot: THREE.Mesh;
  rightFoot: THREE.Mesh;
  extenderArm: THREE.Mesh;

  constructor(player: Player) {
    this.player = player;

    this.gameManager = GameManager.getInstance();
    this.inputManager = InputManager.getInstance();

    this.extendLegs = this.extendLegs.bind(this);
    this.releaseLegs = this.releaseLegs.bind(this);
    this.jump = this.jump.bind(this);
    this.inputManager.on('extend.legs', this.extendLegs);
    this.inputManager.on('release.legs', this.releaseLegs);
    this.inputManager.on('jump.legs', this.jump);

    this.currentSegments = 0;

    this.parentGroup = new THREE.Group();
    this.parentGroup.position.y = -Player.SIZE / 2;
    this.player.object?.add(this.parentGroup);

    this.leftGroup = new THREE.Group();
    this.leftGroup.position.x = -0.25;
    this.parentGroup.add(this.leftGroup);

    this.rightGroup = new THREE.Group();
    this.rightGroup.position.x = 0.25;
    this.parentGroup.add(this.rightGroup);

    const legGeometry = new THREE.BoxGeometry(0.1, 0.1, 0.1);
    const material = new THREE.MeshStandardMaterial({
      color: 0xb0916e,
      roughness: 0.6,
    });

    this.leftLeg = new THREE.Mesh(legGeometry, material);
    this.leftLeg.position.y = -0.05;
    this.leftGroup.add(this.leftLeg);

    this.rightLeg = new THREE.Mesh(legGeometry, material);
    this.rightLeg.position.y = -0.05;
    this.rightGroup.add(this.rightLeg);

    const footGeometry = new THREE.BoxGeometry(0.1, 0.1, 0.25);

    this.leftFoot = new THREE.Mesh(footGeometry, material);
    this.leftFoot.position.z = -0.125;
    this.leftFoot.position.y = -0.1;
    this.leftGroup.add(this.leftFoot);

    this.rightFoot = new THREE.Mesh(footGeometry, material);
    this.rightFoot.position.z = -0.125;
    this.rightFoot.position.y = -0.1;
    this.rightGroup.add(this.rightFoot);

    const armGeometry = new THREE.BoxGeometry(0.25, 0.1, 0.1);
    this.extenderArm = new THREE.Mesh(armGeometry, material);
    this.extenderArm.position.x = Player.SIZE / 2 + 0.125;
    this.extenderArm.position.y = Player.SIZE / 2;
    this.parentGroup.add(this.extenderArm);
  }

  extendLegs(): void {
    if (this.player.body === undefined) {
      return;
    }
    if (this.currentSegments >= ExtendoLegs.MAX_SEGMENTS) {
      return;
    }

    // janky way of doing slow movement for now... I'll make it better later
    Player.MOVEMENT_FORCE = 5.5;

    this.currentSegments++;

    this.player.body.addShape(
      new CANNON.Sphere(ExtendoLegs.SEGMENT_LENGTH / 2),
      new CANNON.Vec3(
        0,
        -Player.SIZE / 2 - ExtendoLegs.SEGMENT_LENGTH * (this.currentSegments - 0.5),
        0,
      ),
    );
    this.player.body.position.y += ExtendoLegs.SEGMENT_LENGTH * 1.5;

    const legScale = this.currentSegments * ExtendoLegs.SEGMENT_LENGTH * 10;
    const legPosition = -(this.currentSegments * ExtendoLegs.SEGMENT_LENGTH) / 2;
    this.leftLeg.scale.y = legScale;
    this.rightLeg.scale.y = legScale;
    this.leftLeg.position.y = legPosition;
    this.rightLeg.position.y = legPosition;

    const footPosition = 0.1 -this.currentSegments * ExtendoLegs.SEGMENT_LENGTH;
    this.leftFoot.position.y = footPosition;
    this.rightFoot.position.y = footPosition;
  }

  releaseLegs(): void {
    if (this.player.body === undefined) {
      return;
    }

    // janky way of doing slow movement for now... I'll make it better later
    Player.MOVEMENT_FORCE = 5.75;

    this.currentSegments = 0;

    const shapesToRemove: CANNON.Shape[] = [];
    for (let i = 1; i < this.player.body.shapes.length; i++) {
      shapesToRemove.push(this.player.body.shapes[i]);
    }
    for (const shape of shapesToRemove) {
      this.player.body.removeShape(shape);
    }
    this.leftLeg.scale.y = 1;
    this.rightLeg.scale.y = 1;
    this.leftLeg.position.y = -0.05;
    this.rightLeg.position.y = -0.05;
    this.leftFoot.position.y = -0.1;
    this.rightFoot.position.y = -0.1;
  }
  
  jump(): void {
    if (this.player.body === undefined) {
      return;
    }

    this.releaseLegs();
    this.player.body.applyLocalForce(new CANNON.Vec3(
      0,
      ExtendoLegs.JUMPING_FORCE,
      -ExtendoLegs.JUMPING_FORCE,
    ));
  }

  destroy(): void {
    this.releaseLegs();
    this.parentGroup.removeFromParent();
    this.inputManager.off('extend.legs');
    this.inputManager.off('release.legs');
    this.inputManager.off('jump.legs');
  }
}
