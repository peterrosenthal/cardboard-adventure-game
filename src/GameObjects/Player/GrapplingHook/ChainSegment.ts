import * as THREE from 'three';
import * as CANNON from 'cannon-es';

interface ChainSegment {
  object: THREE.Mesh;
  body: CANNON.Body;
  constraint: CANNON.Constraint | CANNON.Constraint[];
}

export default ChainSegment;
