import * as THREE from 'three';
import * as CANNON from 'cannon-es';
import GameManager from '../../../Managers/GameManager';
import InputManager from '../../../Managers/InputManager';
import Player from '../Player';
import ChainSegment from './ChainSegment';

export default class GrapplingHook {
  static MAX_DISTANCE = 12;
  static MIN_SEGMENT_LENGTH = 0.2;

  private player: Player;
  private gameManager: GameManager;
  private inputManager: InputManager;
  private raycaster: THREE.Raycaster;

  object: THREE.Mesh;

  chainGeometry: THREE.SphereGeometry;
  chainMaterial: THREE.MeshStandardMaterial;

  chainSegments: ChainSegment[];

  attachedObject?: THREE.Object3D;
  attachedBody?: CANNON.Body;

  constructor(player: Player) {
    this.player = player;
    this.gameManager = GameManager.getInstance();

    this.object = new THREE.Mesh(
      new THREE.BoxGeometry(Player.SIZE / 4, Player.SIZE / 4, Player.SIZE / 2),
      new THREE.MeshStandardMaterial({
        color: 0xb0916e,
        roughness: 0.9,
      }),
    );
    this.object.position.set(5 * Player.SIZE / 8, 0, 0);
    this.object.rotation.y = -Math.PI;
    this.object.name = 'grapplinghook';

    this.player.object?.add(this.object);

    this.raycaster = new THREE.Raycaster();

    this.chainGeometry = new THREE.SphereGeometry();
    this.chainMaterial = new THREE.MeshStandardMaterial({
      color: 0x1a1f23,
    });

    this.chainSegments = [];

    this.castLine = this.castLine.bind(this);
    this.shortenChain = this.shortenChain.bind(this);
    this.inputManager = InputManager.getInstance();
    this.inputManager.on('cast.grapple', this.castLine);
    this.inputManager.on('shorten.grapple', this.shortenChain);
  }

  update(): void {
    if (this.attachedObject !== undefined &&
        this.attachedBody !== undefined) {
      for (const segment of this.chainSegments) {
        const localPosition = new THREE.Vector3()
          .copy(segment.body.position as unknown as THREE.Vector3);
        this.object.worldToLocal(localPosition);

        const worldQuaternion = new THREE.Quaternion()
          .copy(segment.body.quaternion as unknown as THREE.Quaternion);
        const parentWorldQuaternion = new THREE.Quaternion();
        this.object.getWorldQuaternion(parentWorldQuaternion);
        const localQuaternion = worldQuaternion
          .clone()
          .multiply(parentWorldQuaternion.invert());

        segment.object.position.copy(localPosition);
        segment.object.quaternion.copy(localQuaternion);
      }
    }
  }

  private castLine(): void {
    if (this.attachedBody !== undefined && this.attachedObject !== undefined)  {
      this.detatch();
      return;
    }

    const position = new THREE.Vector3();
    const direction = new THREE.Vector3();

    this.object.getWorldPosition(position);
    this.object.getWorldDirection(direction);

    this.raycaster.set(position, direction);

    const intersects = this.raycaster.intersectObject(this.gameManager.scene, true);
    if (intersects.length <= 0) {
      return;
    }
    const closestIntersectPosition = new THREE.Vector3();
    let closestIntersectDistance: number | undefined;
    let closestIntersectObject: THREE.Object3D | undefined;
    let closestIntersectBody: CANNON.Body | undefined;
    for (const intersect of intersects) {
      if (intersect.object !== this.object && intersect.object.type !== 'Line') {
        if (closestIntersectDistance === undefined ||
            closestIntersectObject === undefined ||
            closestIntersectBody === undefined ||
            intersect.distance < closestIntersectDistance) {
          const physicsObject = this.gameManager.findPhysicsObject(intersect.object);
          if (physicsObject !== undefined && physicsObject.body !== undefined) {
            closestIntersectPosition.copy(intersect.point);
            closestIntersectDistance = intersect.distance;
            closestIntersectObject = intersect.object;
            closestIntersectBody = physicsObject.body;
          }
        }
      }
    }

    if (closestIntersectDistance === undefined ||
        closestIntersectObject === undefined ||
        closestIntersectBody === undefined) {
      return;
    }

    if (closestIntersectDistance > GrapplingHook.MAX_DISTANCE) {
      return;
    }
    
    this.attachedObject = closestIntersectObject;
    this.attachedBody = closestIntersectBody;

    const originLocal = new THREE.Vector3(0, 0, Player.SIZE / 4);
    const targetLocal = closestIntersectPosition.clone();
    this.object.worldToLocal(targetLocal);

    const originWorld = originLocal.clone();
    const targetWorld = closestIntersectPosition.clone();
    this.object.localToWorld(originWorld);

    const numSegments =
        Math.floor(closestIntersectDistance / GrapplingHook.MIN_SEGMENT_LENGTH);
    const segmentLength = closestIntersectDistance / numSegments;

    (this.gameManager.world.solver as CANNON.GSSolver | CANNON.SplitSolver).iterations =
      numSegments * 2;

    const shape = new CANNON.Sphere(segmentLength / 20);
    for (let i = 0; i < numSegments; i++) {
      // get local and world positions of this segment
      const t = (i + 0.5) / numSegments;
      const positionLocal = originLocal
        .clone()
        .lerp(targetLocal, t);
      const positionWorld = originWorld
        .clone()
        .lerp(targetWorld, t);
      
      // add visual element of chain
      const object = new THREE.Mesh(this.chainGeometry, this.chainMaterial);
      object.position.copy(positionLocal);
      object.scale.set(segmentLength / 2, segmentLength / 2, segmentLength / 2);
      object.name = `chainsegment${i}`;
      this.object.add(object);

      // add physics element of chain
      const body = new CANNON.Body({ mass: 0.01, shape, linearDamping: 0.5 });
      body.position.copy(positionWorld as unknown as CANNON.Vec3);
      const segmentWorldQuaternion = new THREE.Quaternion();
      object.getWorldQuaternion(segmentWorldQuaternion);
      body.quaternion.set(
        segmentWorldQuaternion.x,
        segmentWorldQuaternion.y,
        segmentWorldQuaternion.z,
        segmentWorldQuaternion.w,
      );
      this.gameManager.world.addBody(body);

      if (i === 0) {
        // add first chain
        const positionPlayer = positionWorld.clone();
        this.player.object!.worldToLocal(positionPlayer);
        const constraint = new CANNON.PointToPointConstraint(
          this.player.body!,
          positionPlayer as unknown as CANNON.Vec3,
          body,
          new CANNON.Vec3(0, 0, -segmentLength / 2),
          1e150,
        );
        constraint.collideConnected = false;
        this.gameManager.world.addConstraint(constraint);

        this.chainSegments.push({ object, body, constraint });
      } else if (i === numSegments - 1) {
        // add last chain
        const constraint: CANNON.Constraint[] = [];
        constraint.push(new CANNON.DistanceConstraint(
          this.chainSegments[i - 1].body,
          body,
          undefined,
          1e10,
        ));
        constraint[0].collideConnected = false;
        constraint[0].enable();
        constraint[0].update();
        this.gameManager.world.addConstraint(constraint[0]);

        const targetBodyPoint = targetWorld.clone();
        this.attachedObject.worldToLocal(targetBodyPoint);
        console.log(targetBodyPoint);

        constraint.push(new CANNON.LockConstraint(
          body,
          this.attachedBody,
          { maxForce: 1e200 },
        ));
        (constraint[1] as CANNON.LockConstraint).pivotA = new CANNON.Vec3();
        (constraint[1] as CANNON.LockConstraint).pivotB =
          targetBodyPoint as unknown as CANNON.Vec3;
        constraint[1].update();
        this.gameManager.world.addConstraint(constraint[1]);

        this.chainSegments.push({ object, body, constraint });
      } else {
        // add the middle chains
        const constraint = new CANNON.DistanceConstraint(
          this.chainSegments[i - 1].body,
          body,
          undefined,
          1e50,
        );
        constraint.collideConnected = false;
        constraint.enable();
        constraint.update();
        this.gameManager.world.addConstraint(constraint);

        this.chainSegments.push({ object, body, constraint });
      }
    }
    console.log(this.chainSegments);
  }

  private detatch(): void {
    if (this.attachedObject === undefined ||
        this.attachedBody === undefined) {
      return;
    }
    this.attachedBody = undefined;
    this.attachedObject = undefined;
    
    for (const segment of this.chainSegments) {
      if (segment.constraint instanceof Array) {
        for (const constraint of segment.constraint) {
          this.gameManager.world.removeConstraint(constraint);
        }
      } else {
        this.gameManager.world.removeConstraint(segment.constraint);
      }
      this.gameManager.world.removeBody(segment.body);
      segment.object.removeFromParent();
    }

    this.chainSegments = [];
  }

  private shortenChain(): void {
    if (this.chainSegments.length === 0) {
      return;
    }
    
    if (this.chainSegments.length > 2) {
      const first = this.chainSegments[0];
      const second = this.chainSegments[1];
      const third = this.chainSegments[2];

      const constraint = new CANNON.DistanceConstraint(
        first.body,
        third.body,
        first.object.scale.x,
        1e10,
      );
      this.gameManager.world.addConstraint(constraint);
      if (third.constraint instanceof Array) {
        this.gameManager.world.removeConstraint(third.constraint[0]);
        third.constraint[0] = constraint;
      } else {
        this.gameManager.world.removeConstraint(third.constraint);
        third.constraint = constraint;
      }
      this.gameManager.world.removeConstraint(second.constraint as CANNON.Constraint);
      this.gameManager.world.removeBody(second.body);
      second.object.removeFromParent();

      this.chainSegments.splice(1, 1);
    } else {
      this.detatch();
    }
  }

  destroy(): void {
    this.detatch();
    this.object.removeFromParent();
    this.inputManager.off('cast.grapple');
    this.inputManager.off('shorten.grapple');
  }
}
