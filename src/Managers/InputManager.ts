import * as THREE from 'three';
import EventEmitter from '../Utils/EventEmitter';

export default class InputManager extends EventEmitter {
  private static S?: InputManager;
  static getInstance(): InputManager {
    if (!(this.S instanceof InputManager)) {
      this.S = new InputManager();
    }
    return this.S;
  }

  private keys: string[];
  private shorteningGrapple: boolean;

  private constructor() {
    super();

    this.keys = [];
    this.shorteningGrapple = false;

    this.pushKeys = this.pushKeys.bind(this);
    this.filterKeys = this.filterKeys.bind(this);
    window.addEventListener('keydown', this.pushKeys);
    window.addEventListener('keyup', this.filterKeys);
  
    const ws = new WebSocket('wss://still-sunset-7792.fly.dev/');

    // put beholder stuff here for now
    fetch('https://still-sunset-7792.fly.dev/rooms', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ 'hostPeerId': '' }),
    })
      .then(response => response.json())
      .then((v) => {
        console.log(v);
        const roomId = v.roomId;

        setTimeout(() => {
          // connects to the server thing
          ws.send(JSON.stringify({
            'type': 'join',
            'roomId': roomId,
            'message': 'connect from client',
          }));
        },
        300,
        );
      });

    // listening to websocket
    ws.addEventListener('message', (e) => {
      console.log(e);
      e.data.text().then((t: string) => {
        console.log(t);
        const json = JSON.parse(t);
        const action = json.message.action;
        const data = json.message.data;
        // check for grappling hook events
        switch (action) {
          case 'GRAPPLE_TRIGGER_PRESS':
            this.trigger('cast.grapple');
            break;
          case 'GRAPPLE_BUTTON_PRESS':
            this.shorteningGrapple = true;
            this.shortenGrapple();
            break;
          case 'GRAPPLE_BUTTON_RELEASE':
            this.shorteningGrapple = false;
            break;
          case 'JOYSTICK_UPDATE':
          {
            const movementVector = new THREE.Vector2(data[0], data[1]).divideScalar(50);
            if (movementVector.length() > 0.05) {
              this.trigger('forward.player', movementVector.y);
              this.trigger('right.player', movementVector.x);
            }
            this.trigger('counterclockwise.player', data[2] * 2);
            break;
          }
          default:
            break;
        }
      });
    });

    this.shortenGrapple = this.shortenGrapple.bind(this);
  }

  private async shortenGrapple(): Promise<void> {
    this.trigger('shorten.grapple');

    if (this.shorteningGrapple) {
      window.setTimeout(this.shortenGrapple, 200);
    }
  }

  private pushKeys(event: KeyboardEvent): void {
    const code = event.code;
    if (!this.findCodes(code)) {
      switch(code) {
        case 'KeyW':
        case 'ArrowUp':
          if (!this.findCodes('KeyW', 'ArrowUp')) {
            this.trigger('forward.player', 1);
          }
          break;
        case 'KeyA':
        case 'ArrowLeft':
          if (!this.findCodes('KeyA', 'ArrowLeft')) {
            this.trigger('left.player', 1);
          }
          break;
        case 'KeyS':
        case 'ArrowDown':
          if (!this.findCodes('KeyS', 'ArrowDown')) {
            this.trigger('back.player', 1);
          }
          break;
        case 'KeyD':
        case 'ArrowRight':
          if (!this.findCodes('KeyD', 'ArrowRight')) {
            this.trigger('right.player', 1);
          }
          break;
        case 'KeyQ':
        case 'Comma':
          if (!this.findCodes('KeyQ', 'Comma')) {
            this.trigger('counterclockwise.player', 1);
          }
          break;
        case 'KeyE':
        case 'Period':
          if (!this.findCodes('KeyE', 'Period')) {
            this.trigger('clockwise.player', 1);
          }
          break;
        case 'Space':
          if (!this.findCodes('Space')) {
            this.trigger('cast.grapple');
            this.trigger('extend.legs');
          }
          break;
        case 'KeyP':
          if (!this.findCodes('KeyP')) {
            this.trigger('shorten.grapple');
            this.trigger('release.legs');
          }
          break;
        case 'KeyO':
          if (!this.findCodes('KeyO')) {
            this.trigger('jump.legs');
          }
          break;
        case 'Digit1':
          if (!this.findCodes('Digit1')) {
            this.trigger('equip.grapple');
          }
          break;
        case 'Digit2':
          if (!this.findCodes('Digit2')) {
            this.trigger('equip.legs');
          }
          break;
        default:
          break;
      }
      this.keys.push(code);
    }
  }

  private filterKeys(event: KeyboardEvent): void {
    const code = event.code;
    switch(code) {
      case 'KeyW':
      case 'ArrowUp':
        if (this.findCodes('KeyW', 'ArrowUp')) {
          this.trigger('forward.player', 0);
        }
        this.keys = this.keys.filter(function(key) {
          return key !== 'KeyW' && key !== 'ArrowUp';
        });
        break;
      case 'KeyA':
      case 'ArrowLeft':
        if (this.findCodes('KeyA', 'ArrowLeft')) {
          this.trigger('left.player', 0);
        }
        this.keys = this.keys.filter(function(key) {
          return key !== 'KeyA' && key !== 'ArrowLeft';
        });
        break;
      case 'KeyS':
      case 'ArrowDown':
        if (this.findCodes('KeyS', 'ArrowDown')) {
          this.trigger('back.player', 0);
        }
        this.keys = this.keys.filter(function(key) {
          return key !== 'KeyS' && key !== 'ArrowDown';
        });
        break;
      case 'KeyD':
      case 'ArrowRight':
        if (this.findCodes('KeyD', 'ArrowRight')) {
          this.trigger('right.player', 0);
        }
        this.keys = this.keys.filter(function(key) {
          return key !== 'KeyD' && key !== 'ArrowRight';
        });
        break;
      case 'KeyQ':
      case 'Comma':
        if (this.findCodes('KeyQ', 'Comma')) {
          this.trigger('counterclockwise.player', 0);
        }
        this.keys = this.keys.filter(function(key) {
          return key !== 'KeyQ' && key !== 'Comma';
        });
        break;
      case 'KeyE':
      case 'Period':
        if (this.findCodes('KeyE', 'Period')) {
          this.trigger('clockwise.player', 0);
        }
        this.keys = this.keys.filter(function(key) {
          return key !== 'KeyE' && key !== 'Period';
        });
        break;
      default:
        this.keys = this.keys.filter(function(key) { return key !== code; });
        break;
    }
  }

  private findCodes(...codes: string[]): boolean {
    for (const code of codes) {
      for (const key of this.keys) {
        if (code === key) {
          return true;
        }
      }
    }
    return false;
  }
}
