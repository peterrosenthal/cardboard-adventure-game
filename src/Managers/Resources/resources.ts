import Resource from './Resource';
import ResourceType from './ResourceType';

const resources: Resource[] = [
  {
    name: 'playerArrayTextures',
    type: ResourceType.ARRAY_TEXTURES,
    path: [
      'resources/textures/player/px.png',
      'resources/textures/player/nx.png',
      'resources/textures/player/py.png',
      'resources/textures/player/ny.png',
      'resources/textures/player/pz.png',
      'resources/textures/player/nz.png',
    ],
  },
  {
    name: 'floorCheckerboardTexture',
    type: ResourceType.TEXTURE,
    path: 'resources/textures/floor/checkerboard.jpg',
  },
  {
    name: 'floorGrassTexture',
    type: ResourceType.TEXTURE,
    path: 'resources/textures/floor/grass.jpg',
  },
];

export default resources;
