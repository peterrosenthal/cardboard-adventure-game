import * as THREE from 'three';
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import EventEmitter from '../../Utils/EventEmitter';
import resources from './resources';
import ResourceLoaders from './ResourceLoaders';
import Resource from './Resource';
import ResourceType from './ResourceType';

export default class ResourceManager extends EventEmitter {
  private static S?: ResourceManager;
  static getInstance(): ResourceManager {
    if (!(this.S instanceof ResourceManager)) {
      this.S = new ResourceManager();
    }
    return this.S;
  }

  items: Record<string, GLTF | THREE.Texture | THREE.CubeTexture | THREE.Texture[]>;
  toLoad: number;
  loaded: number;
  
  private loaders: ResourceLoaders;

  private constructor() {
    super();

    this.items = {};
    this.toLoad = resources.length;
    this.loaded = 0;

    this.loaders = {
      gltfLoader: new GLTFLoader(),
      textureLoader: new THREE.TextureLoader(),
      cubeTextureLoader: new THREE.CubeTextureLoader(),
    };
  }

  loadResources() {
    for (const resource of resources) {
      switch (resource.type) {
        case ResourceType.GLTF_MODEL:
          if (typeof resource.path !== 'string') {
            break;
          }
          this.loaders.gltfLoader.load(
            resource.path,
            (gltf: GLTF) => this.resourceLoaded(resource, gltf),
          );
          break;
        case ResourceType.TEXTURE:
          if (typeof resource.path !== 'string') {
            break;
          }
          this.loaders.textureLoader.load(
            resource.path,
            (texture: THREE.Texture) => this.resourceLoaded(resource, texture),
          );
          break;
        case ResourceType.CUBE_TEXTURE:
          if (!(resource.path instanceof Array)) {
            break;
          }
          this.loaders.cubeTextureLoader.load(
            resource.path,
            (texture: THREE.CubeTexture) => this.resourceLoaded(resource, texture),
          );
          break;
        case ResourceType.ARRAY_TEXTURES:
          if (!(resource.path instanceof Array)) {
            break;
          }
          {
            const textures: Array<THREE.Texture | undefined> = [];
            // have to use "old school" for loop to ensure order is preserved
            for (let i = 0; i < resource.path.length; i++) {
              textures.push(undefined);
              const path = resource.path[i];
              this.loaders.textureLoader.load(
                path,
                (texture: THREE.Texture) => {
                  textures[i] = (texture);
                  if (textures.length === resource.path.length) {
                    let allTexturesLoaded = true;
                    for (const texture of textures) {
                      if (!(texture instanceof THREE.Texture)) {
                        allTexturesLoaded = false;
                      }
                    }
                    if (allTexturesLoaded) {
                      this.resourceLoaded(resource, textures as THREE.Texture[]);
                    }
                  }
                },
              );
            }
          }
          break;
        default:
          break;
      }
    }
  }

  private resourceLoaded(
    resource: Resource,
    loaded: GLTF | THREE.Texture | THREE.CubeTexture | THREE.Texture[],
  ) {
    this.items[resource.name] = loaded;

    this.loaded++;

    if (this.loaded === this.toLoad) {
      this.allResourcesLoaded();
    }
  }

  private allResourcesLoaded() {
    this.trigger('loaded');
  }
}
