import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

interface ResourceLoaders {
  gltfLoader: GLTFLoader,
  textureLoader: THREE.TextureLoader,
  cubeTextureLoader: THREE.CubeTextureLoader,
}

export default ResourceLoaders;
