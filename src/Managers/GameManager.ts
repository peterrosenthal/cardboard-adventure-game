import * as CANNON from 'cannon-es';
import * as THREE from 'three';
import ExampleObstacle from '../GameObjects/Environment/ExampleObstacle';
import Floor from '../GameObjects/Environment/Floor';
import GameObject from '../GameObjects/GameObject';
import PhysicsObject from '../GameObjects/PhysicsObject';
import Player from '../GameObjects/Player/Player';
import Lights from '../GameObjects/Environment/Lights';
import SizeManager from './SizeManager';
import TimeManager from './TimeManager';
import ResourceManager from './Resources/ResourceManager';
import PressurePlate from '../GameObjects/Environment/PressurePlate';
import LiftableWall from '../GameObjects/Environment/LiftableWall';

export default class GameManager {
  private static S?: GameManager;
  static getInstance(): GameManager {
    if (!(this.S instanceof GameManager)) {
      this.S = new GameManager();
      this.S.resourceManager.loadResources();
    }
    return this.S;
  }

  timeManager: TimeManager;
  sizeManager: SizeManager;
  resourceManager: ResourceManager;

  world: CANNON.World;
  scene: THREE.Scene;

  objects: Record<string, GameObject>;

  player?: Player;
  pressurePlate?: PressurePlate;
  floor?: Floor;
  
  canvas: HTMLCanvasElement;
  camera: THREE.OrthographicCamera;
  renderer: THREE.WebGLRenderer;

  private constructor() {
    // get the canvas dom element
    this.canvas = document.querySelector('canvas')!;

    // time manager
    this.timeManager = TimeManager.getInstance();

    // size manager
    this.sizeManager = SizeManager.getInstance();
    this.resize = this.resize.bind(this);
    this.sizeManager.on('resize', this.resize);

    // cannon world
    this.world = new CANNON.World();
    this.world.gravity.set(0, -9.8, 0);

    // three scene
    this.scene = new THREE.Scene();

    // three camera
    this.camera = new THREE.OrthographicCamera(
      -5 * this.sizeManager.aspectRatio,
      5 * this.sizeManager.aspectRatio,
      5,
      -5,
      -20,
      20,
    );
    this.camera.position.set(2.5, 5, 2.5);
    this.camera.lookAt(new THREE.Vector3());

    // three renderer
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas });
    this.renderer.setSize(this.sizeManager.width, this.sizeManager.height);
    this.renderer.setPixelRatio(this.sizeManager.pixelRatio);

    // objects
    this.objects = {};

    // resource manager goes last to ensure the entire game manager is ready
    // before the init function even has a thought about being called
    // unlike the other managers, the resource manager shouldn't even be initialized
    // in the index.js before the game manager like the other managers should
    this.resourceManager = ResourceManager.getInstance();
    this.init = this.init.bind(this);
    this.resourceManager.on('loaded', this.init);
  }

  /**
   * Many aspects of initialization such as the inclusion of objects has to happen
   * after the constructor is finished. This is accomplished in the init function
   */
  private init(): void {
    // lights
    this.objects.lights = new Lights();
    this.objects.lights.addToScene();

    // objects
    this.floor = new Floor();
    this.floor.addToWorldAndScene();

    this.player = new Player();
    this.player.addToWorldAndScene();

    this.objects.floor = this.floor;
    this.objects.player = this.player;

    // hook up update loop
    this.update = this.update.bind(this);
    this.timeManager.on('update', this.update);

    // test out the pressure plate
    this.pressurePlate = new PressurePlate(
      new THREE.Vector3(0, 0.05, 5),
      new THREE.Vector3(1.1, 0.15, 1.1),
    );

    // object that react to the pressure plate need to go after the
    // pressure plate because of weak coding... I'll fix it sometime later
    this.objects.exampleObstacle = new ExampleObstacle();
    (this.objects.exampleObstacle as ExampleObstacle).addToWorldAndScene();

    this.objects.wall1 = new LiftableWall(
      new THREE.Vector3(-10/3, 5/3, -2),
      new THREE.Vector3(10/3, 10/3, 10/3),
    );
    (this.objects.wall1 as LiftableWall).addToWorldAndScene();

    this.objects.wall2 = new LiftableWall(
      new THREE.Vector3(0, 5/3, -2),
      new THREE.Vector3(10/3, 10/3, 10/3),
      true,
    );
    (this.objects.wall2 as LiftableWall).addToWorldAndScene();

    this.objects.wall3 = new LiftableWall(
      new THREE.Vector3(10/3, 5/3, -2),
      new THREE.Vector3(10/3, 10/3, 10/3),
    );
    (this.objects.wall3 as LiftableWall).addToWorldAndScene();
  }

  private update(): void {
    // update objects
    this.player?.update();
    this.pressurePlate?.update();
    this.objects.wall2.update();

    // step world
    this.world.step(1 / 60, this.timeManager.delta, 3);

    // sync objects world to scene
    this.player?.syncWorldToScene();
    for (const key in this.objects) {
      const object = this.objects[key];
      if (object instanceof PhysicsObject) {
        object.syncWorldToScene();
      }
    }

    // followcam, the player "drags" the camera around
    this.player?.dragCamera();

    // render scene
    this.renderer.render(this.scene, this.camera);
  }

  private resize(): void {
    // resize camera
    this.camera.left = this.camera.bottom * this.sizeManager.aspectRatio;
    this.camera.right = this.camera.top * this.sizeManager.aspectRatio;
    this.camera.updateProjectionMatrix();

    // resize renderer
    this.renderer.setSize(this.sizeManager.width, this.sizeManager.height);
    this.renderer.setPixelRatio(this.sizeManager.pixelRatio);
  }

  findPhysicsObject(threeObject: THREE.Object3D): PhysicsObject | undefined {
    for(const key in this.objects) {
      const object = this.objects[key];
      if (object instanceof PhysicsObject && threeObject === object.object) {
        return object;
      }
    }
    return undefined;
  }
}
