import * as CANNON from 'cannon-es';

// material names
const playerMaterial = new CANNON.Material('player');
playerMaterial.friction = 0.25;
const floorMaterial = new CANNON.Material('floor');
floorMaterial.friction = 0.05;
const exampleObstacleMaterial = new CANNON.Material('obstacle');
exampleObstacleMaterial.friction = 0.01;
const liftableWallMaterial = new CANNON.Material('wall');
liftableWallMaterial.friction = 0.25;

export {
  playerMaterial,
  floorMaterial,
  exampleObstacleMaterial,
  liftableWallMaterial,
};
